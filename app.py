#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import json

from flask import Flask, Response
app = Flask(__name__)


db = MySQLdb.connect (host = 'localhost',user = 'root', passwd = 'start123', db = 'test_py')

def fetch_one_name(cur):
	for name in cur.fetchall():
		yield name

@app.route("/")
def hello():
	cur = db.cursor()
	cur.execute("select name from users where id between 1234 and 1334 order by name")
	json_resp = json.dumps([user for user in fetch_one_name(cur)])
	return Response(json_resp, content_type='text/json; charset=utf-8')


if __name__ == "__main__":
    app.run(debug = True)